# LSD - Laboratório de Sistemas Distribuídos

[Developed for Imaginaria](http://imaginaria.cc)

#Using

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Grunt](http://gruntjs.com/): Run `[sudo] npm install -g grunt-cli`
  * [Bower](http://bower.io): Run `[sudo] npm install -g bower`
